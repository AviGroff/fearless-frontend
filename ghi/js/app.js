function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
      </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);
        // console.log(response);

        if (!response.ok) {
            // figure out what to do when the respone is bad
            console.log("the response is bad?")
            const container = document.querySelector('.container');
            container.innerHTML += `<div>no good</div>`
        } else {
            const data = await response.json();
            let counter = 1;
            for (let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  console.log(details)
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const startDate = new Date(details.conference.starts).toLocaleDateString();
                  const endDate = new Date(details.conference.ends).toLocaleDateString();
                  const location = details.conference.location.name;
                  const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                  const column = document.querySelector('.row');

                  column.innerHTML += html;


            }

        }
        }
    } catch (e) {
        // figure out what to do if an error is raised
        console.log("an error has occured")
    }
});
