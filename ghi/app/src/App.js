import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="presentations/new" element={<PresentationForm/>} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
        {/* <AttendeeForm/> */}
        {/* <ConferenceForm/> */}
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees}/> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
